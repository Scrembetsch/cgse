#ifndef HELPERS_H
#define HELPERS_H

#include "Classes/TransparentPlane.h"
#include <algorithm>

namespace Helpers {
    void bubbleSort(TransparentPlane *arr, int n, const ofCamera &cam);
    void bubbleSort(ofPlanePrimitive *arr, int n, const ofCamera &cam);
    float clamp(float n, float min, float max);
    void SetupCube(TransparentPlane *arr, ofVec3f position, float width, float height, float depth, ofColor color = ofColor(50, 100));
    void handleControls(const bool *arr, ofVec3f &currentPos, const ofVec3f &currentRotation, float speed, float deltaTime);
}

#endif // HELPERS_H
