#include "Helpers.h"
namespace Helpers {
    float clamp(float n, float min, float max){
        return (n < min) ? min : (max < n) ? max : n;
    }

    void handleControls(const bool *arr, ofVec3f &currentPos, const ofVec3f &currentRotation, float speed, float deltaTime){
        if(arr[static_cast<int>('w')]){
            currentPos.z -= speed * cosf(ofDegToRad(currentRotation.x)) * deltaTime;
            currentPos.x -= speed * sinf(ofDegToRad(currentRotation.x)) * deltaTime;
        }
        if(arr[static_cast<int>('s')]){
            currentPos.z += speed * cosf(ofDegToRad(currentRotation.x)) * deltaTime;
            currentPos.x += speed * sinf(ofDegToRad(currentRotation.x)) * deltaTime;
        }
        if(arr[static_cast<int>('a')]){
            currentPos.z += speed * sinf(ofDegToRad(currentRotation.x)) * deltaTime;
            currentPos.x -= speed * cosf(ofDegToRad(currentRotation.x)) * deltaTime;
        }
        if(arr[static_cast<int>('d')]){
            currentPos.z -= speed * sinf(ofDegToRad(currentRotation.x)) * deltaTime;
            currentPos.x += speed * cosf(ofDegToRad(currentRotation.x)) * deltaTime;
        }
    }

    void SetupCube(TransparentPlane *arr, ofVec3f position, float width, float height, float depth, ofColor color){
        float widthHalf = width / 2;
        float heightHalf = height / 2;
        float depthHalf = depth / 2;
        //front
        arr[0].setPosition(position.x, position.y, position.z + depthHalf);
        arr[0].set(width, height);
        arr[0].planeColor = color;
        //right
        arr[1].setPosition(position.x + widthHalf, position.y, position.z);
        arr[1].set(depth, height);
        arr[1].panDeg(-90);
        arr[1].planeColor = color;
        //back
        arr[2].setPosition(position.x, position.y, position.z - depthHalf);
        arr[2].set(width, height);
        arr[2].planeColor = color;
        //left
        arr[3].setPosition(position.x - widthHalf, position.y, position.z);
        arr[3].set(depth, height);
        arr[3].panDeg(90);
        arr[3].planeColor = color;
        //up
        arr[4].setPosition(position.x, position.y + heightHalf, position.z);
        arr[4].set(width, depth);
        arr[4].tiltDeg(90);
        arr[4].planeColor = color;
        //lower
        arr[5].setPosition(position.x, position.y - heightHalf, position.z);
        arr[5].set(width, depth);
        arr[5].tiltDeg(-90);
        arr[5].planeColor = color;
    }

    void bubbleSort(ofPlanePrimitive *arr, int n, const ofCamera &cam){
        bool swapp = true;
        while(swapp){
            swapp = false;
            for (int i = 0; i < n-1; i++) {
                ofVec3f cPos1 = arr[i].getPosition();
                ofVec3f cPos2 = arr[i+1].getPosition();
                if (cPos1.squareDistance(cam.getPosition()) < cPos2.squareDistance(cam.getPosition())){
                    std::swap(arr[i], arr[i+1]);
                    swapp = true;
                }
            }
        }
    }

    void bubbleSort(TransparentPlane *arr, int n, const ofCamera &cam){
        bool swapp = true;
        while(swapp){
            swapp = false;
            for (int i = 0; i < n-1; i++) {
                ofVec3f cPos1 = arr[i].getPosition();
                ofVec3f cPos2 = arr[i+1].getPosition();
                if (cPos1.squareDistance(cam.getPosition()) < cPos2.squareDistance(cam.getPosition())){
                    std::swap(arr[i], arr[i+1]);
                    swapp = true;
                }
            }
        }
    }
}
