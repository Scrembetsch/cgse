#pragma once

#include "ofMain.h"
#include "ofxAssimpModelLoader.h"
#include "Classes/TransparentPlane.h"
#include "Classes/ObjectHolder.h"
#include "Helpers/Helpers.h"
#include <vector>
#include "GLFW/glfw3.h"

#define kMoveInc 200
#define mapWidth 300
#define mapDepth 300
#define numOfTrees 100
#define numOfGrass 1000
#define numOfClouds 50
#define skyWidth 5000
#define skyDepth 5000
#define minSkyHeight 2000
#define maxSkyHeight 3000
#define sunSpeed 5
#define sunHeight 1000
#define basicBrightness 180

class ofApp : public ofBaseApp {
public:
    //OF methods
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    //Custom methods
    void setupTreeLods();
    void setupTree(float x, float y, float z);
    void setupGrass();
    void setupCloud();

    //Light
    ofLight sunLight;
    ofColor sunLightColor;
    ofLight cloudLight;
    ofColor cloudLightColor;

    //Textures
    ofTexture sunTex;
    ofTexture grassTex;
    ofTexture groundTex;
    ofTexture cobbleStoneTex;
    ofTexture cobbleStoneTexNoMip;
    ofTexture cloudTex;

    //Objects
    ofSpherePrimitive sun;
    ofPlanePrimitive ground;
    ofPlanePrimitive grass[numOfGrass];
    ofPlanePrimitive clouds[numOfClouds];
    ofBoxPrimitive cobblestoneCube;
    ofBoxPrimitive cobblestoneCubeNoMip;
    TransparentPlane cubePlanes[6];
    TransparentPlane otherCubePlanes[6];
    ObjectHolder trees;

    //Camera
    ofCamera cam;
    ofVec3f cameraRotation;

    // Controls
    bool keys[255];

    const ofColor nightColor = ofColor(19, 31, 57);
    const ofColor skyColor = ofColor(146, 226, 253);
    ofColor currentSkyColor = skyColor;

    ofShader shader;

};
