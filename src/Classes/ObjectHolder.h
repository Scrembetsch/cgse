#ifndef OBJECTHOLDER_H
#define OBJECTHOLDER_H

#include "ofMain.h"
#include <vector>
#include "ofxAssimpModelLoader.h"

class ObjectHolder
{
public:
    ObjectHolder();
    ofxAssimpModelLoader lods[3];
    void checkAndSetLod(ofCamera &cam);
    void drawFaces();
    vector<ofxAssimpModelLoader> currentObjects;
    vector<int> currentLods;
};

#endif // OBJECTHOLDER_H
