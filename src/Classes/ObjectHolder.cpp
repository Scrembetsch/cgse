#include "ObjectHolder.h"

ObjectHolder::ObjectHolder()
{
}

void ObjectHolder::checkAndSetLod(ofCamera &cam){
    for(size_t i = 0; i < currentObjects.size(); i++){
        float currentDistance = currentObjects[i].getPosition().distance(cam.getPosition());
        int newLod = 3;
        if (currentDistance > 500)
        {
            newLod = 3;
        }
        else if (currentDistance > 250)
        {
            newLod = 2;
        }
        else
        {
            newLod = 1;
        }
        if (newLod != currentLods.at(i))
        {
            ofPoint rot = currentObjects[i].getRotationAxis(0);
            float angle = currentObjects[i].getRotationAngle(0);
            ofPoint scale = currentObjects[i].getScale();
            ofPoint pos = currentObjects[i].getPosition();
            switch (newLod) {
            case 1:
                currentObjects[i] = lods[0];
                break;
            case 2:
                currentObjects[i] = lods[1];
                break;
            case 3:
                currentObjects[i] = lods[2];
                break;
            default:
                break;
            }
            currentLods[i] = newLod;
            currentObjects[i].setRotation(0, angle, rot.x, rot.y, rot.z);
            currentObjects[i].setScale(scale.x, scale.y, scale.z);
            currentObjects[i].setPosition(pos.x, pos.y, pos.z);
        }
    }
}

void ObjectHolder::drawFaces(){
    for(auto i = currentObjects.begin(); i != currentObjects.end(); i++){
        i->drawFaces();
    }
}
