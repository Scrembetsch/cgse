#ifndef TRANSPARENTPLANE_H
#define TRANSPARENTPLANE_H
#include "ofMain.h"

class TransparentPlane : public ofPlanePrimitive
{
public:
    TransparentPlane();
    ofColor planeColor;
};

#endif // TRANSPARENTPLANE_H
