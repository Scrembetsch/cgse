#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
    // Init
    ofSetBackgroundColor(currentSkyColor);
    ofDisableAlphaBlending();
    ofEnableDepthTest();
    ofSetVerticalSync(true);
    ofSetTimeModeFixedRate();
    ofHideCursor();
//    ofSetFullscreen(true);

    // Camera
    cam.setPosition(0, 15, 0);

    // Lights
    ofSetSmoothLighting(true);
    sunLight.enable();
    sunLight.setPosition(ofVec3f(0, sunHeight, 0));
    sunLight.lookAt(ofVec3f(0, 0, 0));
    sunLight.setDirectional();
    sunLightColor.setBrightness(basicBrightness);
    sunLight.setDiffuseColor(sunLightColor);

    cloudLight.enable();
    cloudLight.setPosition(ofVec3f(0, 100, 0));
    cloudLight.lookAt(ofVec3f(0, 500, 0));
    cloudLight.setSpotlight(90);
    cloudLightColor.setBrightness(basicBrightness);
    cloudLight.setDiffuseColor(cloudLightColor);

    // Textures
    ofDisableArbTex();
    ofLoadImage(sunTex, "sun.jpg");
    ofLoadImage(grassTex, "grass.png");
    ofLoadImage(cobbleStoneTexNoMip, "cobblestone.jpg");
    ofLoadImage(cobbleStoneTex, "cobblestone.jpg");
    ofLoadImage(sunTex, "sun.jpg");
    ofLoadImage(groundTex, "ground.jpg");
    ofLoadImage(cloudTex, "cloud.png");
    cobbleStoneTex.generateMipmap();
    cobbleStoneTex.setTextureMinMagFilter(GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
    groundTex.generateMipmap();
    groundTex.setTextureMinMagFilter(GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
    groundTex.setTextureWrap(GL_REPEAT, GL_REPEAT);

    // Setup objects
        // Sun
    sun.setPosition(0, sunHeight, 0);
        // Ground
    ground.setWidth(10000);
    ground.setHeight(10000);
    ground.mapTexCoords(0, 0, 500, 500);
    ground.tiltDeg(90);
        // Trees
    setupTreeLods();
    for(int i = 0; i < numOfTrees; i++){
        setupTree(-mapWidth + std::rand()%(mapWidth*2), 0, -mapDepth + std::rand()%(mapDepth*2));
    }
        // Mipmap objects
    cobblestoneCube.setScale(0.3, 0.3, 0.3);
    cobblestoneCube.setPosition(150, 15, -100);
    cobblestoneCubeNoMip.setScale(0.3, 0.3, 0.3);
    cobblestoneCubeNoMip.setPosition(100, 15, -100);
        // Grass
        setupGrass();
        Helpers::bubbleSort(grass, numOfGrass, cam);
        // Clouds
        setupCloud();
        Helpers::bubbleSort(clouds, numOfClouds, cam);
        // Transparent Cubes
    Helpers::SetupCube(cubePlanes, ofVec3f(40, 20, -100), 10, 15, 20);
    cubePlanes[0].planeColor = ofColor(255, 0, 0, 100);
    cubePlanes[2].planeColor = ofColor(0, 255, 0, 100);
    Helpers::SetupCube(otherCubePlanes, ofVec3f(10, 15, -80), 15, 20, 10);
    otherCubePlanes[0].planeColor = ofColor(0, 255, 0, 100);
    otherCubePlanes[2].planeColor = ofColor(255, 0, 0, 100);

    // Only needed if shaders are used
#ifdef TARGET_OPENGLES
    shader.load("shadersES2/shader");
#else
    if(ofIsGLProgrammableRenderer()){
        shader.load("shadersGL3/shader");
    }else{
        // Use this one
        shader.load("shadersGL2/shader");
    }
#endif
}

//--------------------------------------------------------------
void ofApp::update() {
    // Light and sun
        // Sun movement
    if(keys[static_cast<int>('m')]){
        sun.rotateDeg(15 * ofGetLastFrameTime(), ofVec3f{1,1,0});
        sun.rotateAroundDeg(sunSpeed * ofGetLastFrameTime(), ofVec3f(0, 0, 1), ofVec3f(0, 0, 0));
        sunLight.setPosition(sun.getPosition());
        sunLight.lookAt(ofVec3f(0, 0, 0));
            // Recalculate brightness & set brightness
        float sunPercantage = Helpers::clamp(sun.getY(), 0, sunHeight) / sunHeight;
        sunLightColor.setBrightness(std::pow(sunPercantage, 2) * basicBrightness);
        sunLight.setDiffuseColor(sunLightColor);
        cloudLightColor.setBrightness(sunLightColor.getBrightness());
        cloudLight.setDiffuseColor(cloudLightColor);
            // Recalculate skycolor and set it
        currentSkyColor = skyColor * sunPercantage - ((1 - sunPercantage) * nightColor);
        ofSetBackgroundColor(currentSkyColor);

    }

    // LOD
    trees.checkAndSetLod(cam);

    // Billboards
    for(auto i = 0; i < numOfGrass; i++){
        grass[i].lookAt(cam);
    }
//    for(auto i = clouds.begin(); i != clouds.end(); i++){
//        i->lookAt(cam);
//    }

    // Handle controls
    ofVec3f position = cam.getPosition();
    Helpers::handleControls(keys, position, cameraRotation, kMoveInc, ofGetLastFrameTime());
    cam.setPosition(position);

    // Transparent objects
    // Sort grass & clouds if super computer is used
//    Helpers::bubbleSort(grass, cam);
    Helpers::bubbleSort(clouds, numOfClouds, cam);
    int n = sizeof(cubePlanes)/sizeof (cubePlanes[0]);
    int n1 = sizeof(otherCubePlanes)/sizeof(otherCubePlanes[0]);
    Helpers::bubbleSort(cubePlanes, n, cam);
    Helpers::bubbleSort(otherCubePlanes, n1, cam);
}

//--------------------------------------------------------------
void ofApp::draw() {
    cam.begin();
    // Sun
    ofSetColor(255, 255, 255);
    sunTex.bind();
    sun.draw();
    sunTex.unbind();

    // Trees
    trees.drawFaces();

    // Object without Mipmapping
    cobbleStoneTexNoMip.bind();
    cobblestoneCubeNoMip.draw();
    cobbleStoneTexNoMip.unbind();

    // Object with Mipmapping
    cobbleStoneTex.bind();
    cobblestoneCube.draw();
    cobbleStoneTex.unbind();

    // Ground
    groundTex.bind();
    ground.draw();
    groundTex.unbind();

    // Transparent objects
    ofEnableAlphaBlending();
        // Clouds
    ofSetColor(255, 255, 255);
    cloudTex.bind();
    for(auto cloud : clouds){
        cloud.draw();
    }
    cloudTex.unbind();
        // Grass
    grassTex.bind();
    for(auto g : grass){
        g.draw();
    }
    grassTex.unbind();

        // Cubes
    for(auto plane : cubePlanes){
        ofSetColor(plane.planeColor);
        plane.draw();
    }
    for(auto plane : otherCubePlanes){
        ofSetColor(plane.planeColor);
        plane.draw();
    }
    ofDisableAlphaBlending();
    ofClearAlpha();

    cam.end();

    // Show FPS
    stringstream ss;
    ss << "FPS: " << ofToString(ofGetFrameRate(),0) << endl << endl;
    ofDrawBitmapString(ss.str().c_str(), 20, 20);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
    if(key == static_cast<int>('m')){
        keys[key] = !keys[key];
    }else{
        keys[key] = true;
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {
    if(key == static_cast<int>('m')){
    }else{
        keys[key] = false;
    }
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {
    float middleY = ofGetHeight()/2;
    float middleX = ofGetWidth()/2;
    cam.setOrientation({0,0,0});
    cameraRotation.y += fmod((middleY - y) / 20, 360);
    cameraRotation.x += fmod((middleX - x) / 20, 360);
    cameraRotation.y = Helpers::clamp(cameraRotation.y, -60, 89);
    cam.panDeg(cameraRotation.x);
    cam.tiltDeg(cameraRotation.y);
    glfwSetCursorPos(((ofAppGLFWWindow*)ofGetWindowPtr())->getGLFWWindow(), middleX, middleY);
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}

void ofApp::setupGrass(){
    for(int i = 0; i < numOfGrass; i++){
        grass[i] = ofPlanePrimitive();
        grass[i].setWidth(4 + std::rand()%5);
        grass[i].setHeight(4 + std::rand()%5);
        grass[i].setPosition(-mapWidth + std::rand()%(mapWidth*2), 2, -mapDepth + std::rand()%(mapDepth*2));
        grass[i].lookAt(cam);
    }
}

void ofApp::setupCloud(){
    for(int i = 0; i < numOfClouds; i++){
        clouds[i] = ofPlanePrimitive();
        int size = std::rand()%1000;
        clouds[i].setWidth(80 + size);
        clouds[i].setHeight(80 + size);
        clouds[i].setPosition(-skyWidth + std::rand()%(skyWidth*2), minSkyHeight + std::rand()%(maxSkyHeight - minSkyHeight), -skyDepth + std::rand()%(skyDepth*2));
        clouds[i].lookAt(cam);
        clouds[i].panDeg(180);
    }
}

void ofApp::setupTreeLods(){
    trees.lods[0].loadModel("Birch_LOD_One.obj");
    trees.lods[1].loadModel("Birch_LOD_Two.obj");
    trees.lods[2].loadModel("Birch_LOD_Three.obj");
}

void ofApp::setupTree(float x, float y, float z)
{
    trees.currentObjects.push_back(trees.lods[2]);
    trees.currentLods.push_back(3);
    trees.currentObjects.back().setRotation(0, 180, 1, 0, 0);
    float scaleFactor = 0.15 + 0.01 * static_cast<float>(std::rand()%15);
    trees.currentObjects.back().setScale(scaleFactor, scaleFactor, scaleFactor);
    trees.currentObjects.back().setPosition(x, y, z);
}
