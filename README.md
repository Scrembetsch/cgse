# CGSE Project
Tested on Ubuntu 18.04  
Requires Open Frameworks  


1.  Download Repository
2.  Copy/Move project folder into [OpenFrameworks_Folder]/apps/myApps/
3.  Open or run project
    *  Open and run with Qt Creator
    *  Open terminal in project folder and type `make` and then `make run`

Controls: WASD and mouse  

Created by Harald Tributsch